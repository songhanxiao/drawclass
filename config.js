// 应用全局配置
module.exports = {
 baseUrl: 'http://192.168.2.102:8090',
  // baseUrl: 'http://localhost:8080',
  // 应用信息
  appInfo: {
    // 应用名称
    name: "platform-app",
    // 应用版本
    version: "1.0.0",
    // 应用logo
    logo: "/static/logo.png",
    // 官方网站
    site_url: "https://popsoft.tech",
    // 政策协议
    agreements: [{
        title: "隐私政策",
        url: "https://popsoft.tech/protocol.html"
      },
      {
        title: "用户服务协议",
        url: "https://popsoft.tech/protocol.html"
      }
    ]
  }
}
